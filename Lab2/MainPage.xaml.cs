﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }
        public IList<Course> Courses { get; private set; }
        public MainPage()
        {
            InitializeComponent();
            Courses = new List<Course>();
            Courses.Add(new Course
            {
                Name = "Baguette",
                Location = "SuperU",
                ImageUrl = "baguette.jpeg",
                ImageUrl2 = "1.jpeg"
            });
            Courses.Add(new Course
            {
                Name = "Cheese",
                Location = "Leclerc",
                ImageUrl = "cheese.jpeg",
                ImageUrl2 = "2.jpg"
            });
            Courses.Add(new Course
            {
                Name = "Tomato",
                Location = "Leclerc",
                ImageUrl = "tomato.jpg",
                ImageUrl2 = "2.jpg"
            });
            Courses.Add(new Course
            {
                Name = "Cheeseburger",
                Location = "SuperU",
                ImageUrl = "burger.jpg",
                ImageUrl2 = "2.jpg"
            });
            Courses.Add(new Course
            {
                Name = "Pizza",
                Location = "SuperU",
                ImageUrl = "pizza.jpg",
                ImageUrl2 = "2.jpg"
            });
            BindingContext = this;

        }

        private void Add_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {
                var name = menuitem.BindingContext as string;
                DisplayAlert("Alert", "Add " + name, "Ok");
            }
        }

        private void Delete_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {
                var name = menuitem.BindingContext as string;
                DisplayAlert("Alert", "Delete " + name, "Ok");
            }
        }

        private void Edit_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {
                var name = menuitem.BindingContext as string;
                DisplayAlert("Alert", "Edit " + name, "Ok");
            }
        }


    }
    
}
